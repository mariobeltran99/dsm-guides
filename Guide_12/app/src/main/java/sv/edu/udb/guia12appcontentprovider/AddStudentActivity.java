package sv.edu.udb.guia12appcontentprovider;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddStudentActivity extends AppCompatActivity {

    EditText edtName, edtLastName,edtCarnet;
    Button btnAddStudent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        loadView();
        btnAddStudent.setOnClickListener(v -> {
            add();
            Intent i = new Intent(AddStudentActivity.this,MainActivity.class);
            startActivity(i);
            finish();
        });
    }
    private void loadView(){
        edtName = findViewById(R.id.edtName);
        edtLastName = findViewById(R.id.edtLastName);
        edtCarnet = findViewById(R.id.edtCarnet);
        btnAddStudent = findViewById(R.id.btnAddStudent);
    }
    private  void add(){
        DatabaseHelper admin = new DatabaseHelper(this,"admin",null,1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String name = edtName.getText().toString().trim();
        String last = edtLastName.getText().toString().trim();
        String carnet = edtCarnet.getText().toString().trim();

        ContentValues reg = new ContentValues();
        reg.put(StudentsContract.Columnas.NOMBRE,name);
        reg.put(StudentsContract.Columnas.APELLIDO, last);
        reg.put(StudentsContract.Columnas.CARNET, carnet);
        try {
            bd.insertOrThrow(StudentsContract.STUDENTS,null, reg);
            bd.close();
            Toast.makeText(this,"Estudiante Agregado Exitosamente",Toast.LENGTH_LONG).show();
        }catch (SQLException e){
            Toast.makeText(this,"ERROR BDD!!", Toast.LENGTH_LONG).show();
        }
    }
}