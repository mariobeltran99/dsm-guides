package app.firebase.dbrealtime.datos;

public class Persona {
    private String dui;
    private String nombre;
    private String genero;
    private String fechanac;
    private double peso;
    private double altura;
    String key;

    public Persona(String dui, String nombre, String genero, String fechanac, double peso, double altura) {
        this.dui = dui;
        this.nombre = nombre;
        this.genero = genero;
        this.fechanac = fechanac;
        this.peso = peso;
        this.altura = altura;
    }

    public Persona(){

    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getGenero() {
        return genero;
    }

    public void setFechanac(String fechanac) {
        this.fechanac = fechanac;
    }

    public String getFechanac() {
        return fechanac;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getPeso() {
        return peso;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getAltura() {
        return altura;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }
}
