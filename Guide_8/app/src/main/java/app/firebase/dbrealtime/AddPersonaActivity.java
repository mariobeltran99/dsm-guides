package app.firebase.dbrealtime;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import java.util.Calendar;

import app.firebase.dbrealtime.datos.Persona;

public class AddPersonaActivity extends AppCompatActivity {
    EditText edtDUI, edtNombre, edtFecha, edtPeso, edtAltura;
    RadioButton rbm, rbf;
    private int nYearInit, nMesInit, nDayInit, sYearIni, sMonthInit, sDayInit;
    static final int DATE_ID = 0;
    Calendar C = Calendar.getInstance();
    String key="",nombre="",dui="",accion="", peso="",altura="",fechaNac="", gen="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_persona);
        inicializar();
    }
    private void inicializar() {
        edtNombre = findViewById(R.id.edtNombre);
        edtDUI = findViewById(R.id.edtDUI);
        edtAltura = findViewById(R.id.edtAltura);
        edtPeso = findViewById(R.id.edtPeso);
        rbm = findViewById(R.id.rBM);
        rbf = findViewById(R.id.rBF);
        rbm.isChecked();
        edtFecha = findViewById(R.id.edtFecha);
        sMonthInit = C.get(Calendar.MONTH);
        sDayInit = C.get(Calendar.DAY_OF_MONTH);
        sYearIni = C.get(Calendar.YEAR);
        edtFecha.setOnClickListener((v -> {
            showDialog(DATE_ID);
        }));

        // Obtención de datos que envia actividad anterior
        Bundle datos = getIntent().getExtras();
        key = datos.getString("key");
        dui = datos.getString("dui");
        nombre=datos.getString("nombre");
        gen = datos.getString("genero");
        fechaNac = datos.getString("fechna");
        peso = datos.getString("peso");
        altura = datos.getString("altura");
        accion=datos.getString("accion");
        edtDUI.setText(dui);
        edtNombre.setText(nombre);
        edtFecha.setText(fechaNac);
        edtPeso.setText(peso);
        edtAltura.setText(altura);
        if(gen == "Masculino" || gen == null){
            rbm.isChecked();
        }else{
            rbf.isChecked();
        }
    }
    private void colocarFecha(){
        edtFecha.setText("");
        edtFecha.setText((nMesInit + 1) + "-" + nDayInit + "-" + nYearInit+" ");
    }
    private DatePickerDialog.OnDateSetListener nDateSetListener = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            nYearInit = year;
            nMesInit = month;
            nDayInit = dayOfMonth;
            colocarFecha();
        }
    };
    @Override
    protected Dialog onCreateDialog(int id){
        switch (id){
            case DATE_ID: return new DatePickerDialog(this,nDateSetListener,sYearIni,sMonthInit,sDayInit);
        }
        return null;
    }

    public void guardar(View v){
        String nombre = edtNombre.getText().toString();
        String dui = edtDUI.getText().toString();
        String genero = "";
        String fechnac = edtFecha.getText().toString();
        double pes = Double.parseDouble(edtPeso.getText().toString());
        double alt = Double.parseDouble(edtAltura.getText().toString());
        if(rbm.isChecked()){
            genero = "Masculino";
        }else{
            genero = "Femenino";
        }
        // Se forma objeto persona
        Persona persona = new Persona(dui,nombre,genero,fechnac, pes, alt);

        if (accion.equals("a")) { //Agregar usando push()
            PersonasActivity.refPersonas.push().setValue(persona);
        }
        else // Editar usando setValue
        {
            PersonasActivity.refPersonas.child(key).setValue(persona);
        }
        finish();
    }
    public void cancelar(View v){
        finish();
    }
}